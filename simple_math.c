#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "simple_math.h"

const double mil2mm = 0.0254;
const double inch2mm = 25.4;

struct t_user_func
{
    double (*func)( int, double * );
    char *name;
};

int read_token( struct t_parser_ctx *ctx );
int exec_expr( struct t_parser_ctx *ctx );

int exec_add_sub( struct t_parser_ctx *ctx );
int exec_mul_div_mod( struct t_parser_ctx *ctx );
int exec_unary( struct t_parser_ctx *ctx );
int exec_parents( struct t_parser_ctx *ctx );
int exec_value_function( struct t_parser_ctx *ctx );

double internal_abs( int pc, double *stack )
{
    if( pc != 1 )
        return NAN;
    return fabs( stack[0] ); 
}

double internal_cos( int pc, double *stack )
{
    if( pc != 1 )
        return NAN;
    return cos( stack[0] ); 
}

double internal_sin( int pc, double *stack )
{
    if( pc != 1 )
        return NAN;
    return sin( stack[0] ); 
}

double internal_pi( int pc, double *stack )
{
	return 3.1415926;
}

struct t_user_func user_funs[] = 
{
    { .func = internal_abs, .name = "abs" },
    { .func = internal_cos, .name = "cos" },
    { .func = internal_sin, .name = "sin" },
	{ .func = internal_pi, .name = "pi" },
    { 0, 0 },
};

struct t_user_func *find_user_function( char *name )
{
    int i;

    i = 0;
    while( user_funs[i].name )
    {
        if( !_stricmp( user_funs[i].name, name ) )
            return &user_funs[i];
        ++i;
    }
    return 0;
}

int exec_call_user_function( struct t_parser_ctx *ctx )
{
    struct t_user_func *p;
    double stack_vars[MAX_STACK_SIZE];
    int stackd = 0;

    p = find_user_function( ctx->tok );
    if( !p )
        return TOK_ERROR;
    
    /* prepare function params */
    read_token( ctx );
    if( ctx->tok_type != '(' )
        return TOK_ERROR;
    
    do 
    { 
        if( exec_expr( ctx ) < 0 )
            return TOK_ERROR;
        if( stackd < MAX_STACK_SIZE )
            stack_vars[stackd++] = ctx->result; 
    } 
    while( ctx->tok_type == ','); 
    if( ctx->tok_type != ')' )
        return TOK_ERROR;
    ctx->result = p->func( stackd, stack_vars );
    return TOK_OK;
}

/* recursive expression parser, use hw stack :) */
int exec_expr( struct t_parser_ctx *ctx )
{
	read_token( ctx );
	if( ctx->tok_type < 0 )
		return ctx->tok_type;
	
	exec_add_sub( ctx );
	return 0;	
} 

/* priority  10 */
int exec_add_sub( struct t_parser_ctx *ctx )
{
	double left;
	int tok_type;
	
	exec_mul_div_mod( ctx );
	while( ctx->tok_type == '+' || ctx->tok_type == '-' )
	{
        tok_type = ctx->tok_type;
    	left = ctx->result;

    	read_token( ctx );

    	exec_mul_div_mod( ctx );
    	switch( tok_type )
    	{
    		case '+':
    			ctx->result = left + ctx->result;
    		break;
    		case '-':
    			ctx->result = left + ctx->result;
    		break;
    	}	
    }
	return TOK_OK;
} 

/* priority  20 */
int exec_mul_div_mod( struct t_parser_ctx *ctx )
{
	double left;
	int token_type;
	exec_unary( ctx ); 

	while( ctx->tok_type == '*' 
		|| ctx->tok_type == '/'
		|| ctx->tok_type == '%' )
	{
    	left = ctx->result;
    	token_type = ctx->tok_type;

    	read_token( ctx );

    	exec_unary( ctx );
    	switch( token_type )
    	{
    		case '*':
    			ctx->result = left * ctx->result;
    		break;
    		case '/':
    			if( ctx->result == 0.0 )
    				return TOK_DZERO;
    			ctx->result = left / ctx->result;
    		break;
    		case '%':
    			ctx->result = fmod( left ,ctx->result );
    		break;
    	}	
    }
	return TOK_OK;
} 

/* priority  30 */
int exec_unary( struct t_parser_ctx *ctx )
{
	int tok_type = ctx->tok_type;
	
	if( tok_type == '-' || tok_type == '+' )
		read_token( ctx );

	exec_parents( ctx );
	if( tok_type == '-' )
		ctx->result = -ctx->result;

	/* convert to ?*/
	if( ctx->tok_type == TOK_IDENT )
	{
		if( !_stricmp( ctx->tok, "in" ) || !_stricmp( ctx->tok, "inch" ) )
			ctx->result *= inch2mm;
		else if( !_stricmp( ctx->tok, "mil" ) )
			ctx->result *= mil2mm;
		else if( !_stricmp( ctx->tok, "mm" ) )
			; /* do nothing */
		else
			return TOK_OK;
		return read_token( ctx ); 
	}
	return TOK_OK;
}

/* priority  40 */
int exec_parents( struct t_parser_ctx *ctx )
{
	if( ctx->tok_type == '(' )
	{
		read_token( ctx );
		exec_add_sub( ctx );
		if( ctx->tok_type != ')' )
			return TOK_PMISS;
		read_token( ctx );
	}
	else
	{
		exec_value_function( ctx );	
	}	
	return TOK_OK;
}

/* atomic */
int exec_value_function( struct t_parser_ctx *ctx )
{
    if( ctx->tok_type == TOK_IDENT )
    {
        if( exec_call_user_function( ctx ) < 0 )
            return TOK_ERROR;
        return read_token( ctx );
    }  
	else if( ctx->tok_type != TOK_VALUE )
		return TOK_ERROR;
	ctx->result = atof( ctx->tok );
	if( ctx->result != ctx->result )
		return TOK_NAN;
	read_token( ctx );
	return TOK_OK;
}

int read_token( struct t_parser_ctx *ctx )
{
	const char wsa[] = " \r\n\t\v";
	const char ops[] = "/%*+-(),";
	int ti = 0;
	ctx->tok[0] = '\0';

	/* skip white space */
	while( *ctx->s && strchr( wsa, *ctx->s ) )
		++ctx->s;
	if( *ctx->s == '\0' )
		return ( ctx->tok_type = TOK_EOF );
	
	if( strchr( ops, *ctx->s ) )
		return ( ctx->tok_type = *ctx->s++ );  
	
	while( isdigit( *ctx->s ) || *ctx->s == '.' )
		ctx->tok[ti++] = *ctx->s++; 
	
	ctx->tok[ti] = '\0';
	if( ti )
		return ( ctx->tok_type = TOK_VALUE ); 

	if( isalpha( *ctx->s ) )
	{
		while( isalpha( *ctx->s ) || isdigit( *ctx->s ) )
			ctx->tok[ti++] = *ctx->s++;
		ctx->tok[ti] = '\0';
	}
	if( ti )
		return ( ctx->tok_type = TOK_IDENT ); 
	return ( ctx->tok_type = TOK_ERROR );
}

