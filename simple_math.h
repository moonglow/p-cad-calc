#pragma once

#define MAX_STACK_SIZE 8
#define MAX_TOKEN_SIZE 32

#define TOK_EOF 	-1
#define TOK_ERROR	-2
#define TOK_PMISS	-3
#define TOK_DZERO	-4
#define TOK_NAN		-5

#define TOK_OK		0
#define TOK_VALUE	129
#define TOK_IDENT	130

struct t_parser_ctx
{
	int  tok_type;
	char tok[MAX_TOKEN_SIZE];
	char *s;
	double result;
};

int exec_expr( struct t_parser_ctx *ctx );
