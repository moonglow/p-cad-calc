P-CAD Calc
=========

P-CAD Calc is a "plugin" for P-CAD 2006(4) what will allow user to calculate expression inside Edit box.

### How to get precompiled version?
Just [Download] it and install

### How to compile it ?
Use [Pelles C] freeware C compiler

### How to install it?
Just rename **implode.dll** to **implode_chain.dll** ( you can find it in P-CAD installation directory ) and copy new **implode.dll** to that place.

### What math engine used to parse and calculate data?
P-CAD Calc use [James Gregson] expression parser, autor [GitHub] page.

[James Gregson]:mailto:james.gregson@gmail.com
[Pelles C]:http://www.smorgasbordet.com/pellesc
[GitHub]:https://github.com/jamesgregson/expression_parser
[Download]:https://bitbucket.org/moonglow/p-cad-calc/downloads