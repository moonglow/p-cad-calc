#include <windows.h>
#include <windowsx.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define OLD_PARSER 1
#define USE_DEBUG_CONSOLE 0

#ifdef OLD_PARSER
	#include "expression_parser.h"
#else
	#include "simple_math.h"
#endif

#pragma comment( lib, "implode_chain.lib") 


#if (USE_DEBUG_CONSOLE==0)
    #define printf( ... )
#endif

/* original implode.dll imports */
extern uint32_t __declspec(dllimport) __cdecl crc32( uint32_t a, uint32_t b, uint32_t c );
extern uint32_t __declspec(dllimport) __cdecl explode( uint32_t a, uint32_t b, uint32_t c, uint32_t d );
extern uint32_t __declspec(dllimport) __cdecl implode( uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e, uint32_t f  );

/* keyboard hook handler */
LRESULT CALLBACK hook_key_press( int nCode, WPARAM wParam, LPARAM lParam );

HHOOK hHook = NULL;

#if USE_DEBUG_CONSOLE
/*redirect output to console */
void enable_console(void)
{
    if( AllocConsole() == FALSE ) 
        return;
    if (!freopen("CONIN$", "w", stdin)) 
        return;
 
    if (!freopen("CONOUT$", "w", stderr)) 
        return;

    if (!freopen("CONOUT$", "w", stdout)) 
        return;

    setbuf(stdout, NULL);
}
#endif


BOOL APIENTRY DllMain(HINSTANCE hInstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            DisableThreadLibraryCalls( hInstDLL );
#if USE_DEBUG_CONSOLE
            enable_console();
#endif
            printf( "pcad calc...\r\n" );
            printf( "GetCurrentThreadId = 0x%.8X\r\n", GetCurrentThreadId() );
            printf( "hInstDLL = 0x%p\r\n", hInstDLL );
            printf( "GetProcessId = 0x%.8X\r\n", GetProcessId( hInstDLL ) );
            hHook = SetWindowsHookEx( WH_KEYBOARD, hook_key_press, hInstDLL, GetCurrentThreadId() );
            printf( "hHook = 0x%p\r\n", hHook );
            break;
        case DLL_THREAD_ATTACH:
            break;
        case DLL_THREAD_DETACH:
            break;
        case DLL_PROCESS_DETACH:
            break;
    }
    return TRUE;
}


LRESULT CALLBACK hook_key_press( int nCode, WPARAM wParam, LPARAM lParam )
{
    HWND hWnd;
    int result;
    char szTempBuffer[0x100];
	double actual_res, expr_res;
	
    if ( nCode != HC_ACTION )
    {
        return CallNextHookEx( hHook, nCode, wParam, lParam ); 
    }
	printf( "0x%.2X 0x%.8X\r\n", wParam, lParam );
    /* we need only PUSH BUTTON action, check for extended flag too */
    if( (lParam>>24) != 0 && (lParam>>24) != 1)
    {
        return CallNextHookEx( hHook, nCode, wParam, lParam );
    }
    /* we need only return key */
    if( wParam != VK_RETURN )
    {
        return CallNextHookEx( hHook, nCode, wParam, lParam );
    }
    /* it is not window ? */
    hWnd = GetFocus();
	printf( "Window handle: %p\r\n", hWnd );
    if( hWnd == NULL )
    {
        return CallNextHookEx( hHook, nCode, wParam, lParam );
    }
    result = GetClassName( hWnd, szTempBuffer, sizeof( szTempBuffer ) ); 
	/* Edit box class? */
	printf( "Window class: %s\r\n", szTempBuffer );
    if( result == 0 || strcmp( szTempBuffer, "Edit" ) )
    {
        return CallNextHookEx( hHook, nCode, wParam, lParam );
    }
    result = GetWindowText( hWnd, szTempBuffer, sizeof( szTempBuffer ) ); 
    if( result == 0 )
    {
        return CallNextHookEx( hHook, nCode, wParam, lParam );
    }
    printf( "expression: %s result ", szTempBuffer );
    
    actual_res = atof( szTempBuffer );
#ifdef OLD_PARSER
    expr_res = parse_expression( szTempBuffer );
    if( (expr_res != expr_res) || ( actual_res == expr_res ) )
    {
        printf( "error\r\n" );
        return CallNextHookEx( hHook, nCode, wParam, lParam ); 
    }
#else
	struct t_parser_ctx ctx;
	ctx.s = szTempBuffer;
	exec_expr( &ctx );
	expr_res = ctx.result;
#endif
    sprintf( szTempBuffer, "%.4f", expr_res );
    printf( "%s\r\n", szTempBuffer );
    Edit_SetText( hWnd, szTempBuffer );
    Edit_SetModify( hWnd, TRUE );
    
    return -1;
}

/* proxy calls */
uint32_t __cdecl hook_crc32( uint32_t a, uint32_t b, uint32_t c )
{
    return crc32( a, b, c );
}

uint32_t __cdecl hook_explode( uint32_t a, uint32_t b, uint32_t c, uint32_t d )
{
    return explode( a, b, c, d ); 
}

uint32_t __cdecl hook_implode( uint32_t a, uint32_t b, uint32_t c, uint32_t d, uint32_t e, uint32_t f  )
{
    return implode( a, b, c, d, e, f );
}

